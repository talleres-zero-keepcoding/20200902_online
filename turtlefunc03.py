import turtle

miTortugaLucera = turtle.Turtle()

def triangulo(l):
    for _ in range(3):
        miTortugaLucera.forward(l)
        miTortugaLucera.left(120)
        
def cuadrado(l):
    for _ in range(4):
        miTortugaLucera.forward(l)
        miTortugaLucera.left(90)


for i in range(6):
    triangulo(100)
    miTortugaLucera.forward(100)
    miTortugaLucera.left(60)

triangulo(100)
triangulo(50)




