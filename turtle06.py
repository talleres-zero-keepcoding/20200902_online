import turtle

miTortugaLucera = turtle.Turtle()

tipo = input("Triángulo o Cuadrado (T/C): ")

if tipo == "T":
    for _ in range(3):
        miTortugaLucera.forward(50)
        miTortugaLucera.left(120)
    
elif tipo == "C":
    for _ in range(4):
        miTortugaLucera.forward(50)
        miTortugaLucera.left(90)

else:
    
    for _ in range(8):
        miTortugaLucera.forward(50)
        miTortugaLucera.left(45)
    
print("Terminé")





