import turtle

miTortugaLucera = turtle.Turtle()

def triangulo():
    for _ in range(3):
        miTortugaLucera.forward(50)
        miTortugaLucera.left(120)
        
def cuadrado():
    for _ in range(4):
        miTortugaLucera.forward(50)
        miTortugaLucera.left(90)

tipo = input("Triángulo o Cuadrado (T/C): ")

if tipo == "T":
    triangulo()    
else:
    cuadrado()
    
print("Terminé")





